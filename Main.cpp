#include <iostream>
#include <iomanip>
#include <string>

int main()
{
	std::cout << "Enter anything: ";
	std::string Anything;
	std::getline(std::cin, Anything);

	std::cout << Anything << " ";
	std::cout << Anything.length() << "\n";

	char FirstChar = Anything.at(0);
	std::cout << FirstChar << " ";

	char LastChar = Anything.at(Anything.length() - 1);
	std::cout << LastChar << " ";

	std::cin;
	return 0;
}